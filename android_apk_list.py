#!/usr/bin/env python
from subprocess import Popen, PIPE
import os
import sys
import fnmatch
import xml.etree.ElementTree as ET
import shutil
import os

xmlNamespace = "http://schemas.android.com/apk/res/android"

def usage():
	print("")
	print("")
	print("Android apk info list")
	print("")
	print("")
	print ("EXAMPLE: ./android_deploy.py .")
	print("")
	print("")
	print("Recursively scans the given folder and finds all *.apk files. It then extracts the files using apktool and parses the AndroidManifest for version code and version name. In the end it gives a list of all found apks and their version info. ")
	print("")
	print("")
	print ("--help prints this help")
	print ("--apktool= custom path to apktool executable")
	print ("")
	pass

def getParam(args, param):
	for i in args:
		if i.startswith("--"+param):
			return i[(len(param)+3):]
	return False

def call(data):
	print( data)
	p = Popen(data, stdout=PIPE, stderr=PIPE)
	output, error = p.communicate()
	
	if error is not None and len(error) > 0:
		print("ERROR:" +error.decode("utf-8"))

	return output

def getManifestVersion(path):
	tree = ET.parse(path)
	root = tree.getroot()
	return root.attrib['{' + xmlNamespace + '}versionCode'], root.attrib['{' + xmlNamespace + '}versionName'], 

def getApktoolExecutable(args):
	param = getParam(args,"apktool")
	if param:
		return param
	if os.name == "posix":
		return "apktool"
	return "apktool.bat"

def main(args):

	if len(args) <= 0:
		usage()
		return

	apks = getApks(args[0],"*.apk")
	output = []

	apktool = getApktoolExecutable(args)

	for a in apks:
		call([apktool,"d",a,"./extracted"])
		versionCode, versionName = getManifestVersion("./extracted/AndroidManifest.xml")
		shutil.move("./extracted", "./extractedRemove")
		shutil.rmtree("./extractedRemove")
		output.append( [a,versionCode, versionName] )

	for o in output:
		print(o)
	pass

def getApks(directory, filter):
	outfiles = []
	for root, dir, files in os.walk(directory):
		for items in fnmatch.filter(files, filter):
			outfiles.append(root + "\\" + items)
	return outfiles



args = sys.argv[1:]


main(args)