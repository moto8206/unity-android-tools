# README #


### Goal ###

Collect all crossplatform tools for use with the unity game engine when deploying to android. 
Android Developer Toolkit are a common requirement for these tools. 
All tools should contain a --help/-h option and should be self explanatory. 

### android_deploy.py ###

Tool to push apk files and obb files to a connected android device. Also handles removal of existing versions and renaming of the obb file. 


### android_apk_list.py ###

Lists all apk in a given directory, extracts them and prints version information for all of them. 